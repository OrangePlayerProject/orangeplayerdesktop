/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.orangeplayer.playerdesktop.sys;

import java.awt.Color;

/**
 *
 * @author martin
 */
public class AppColors {
    //public static final Color PRIMARY_COLOR = Color.decode("#FA6024");
    public static final Color PRIMARY_COLOR = Color.decode("#ff6e40");
    public static final Color SECUNDARY_COLOR = Color.decode("#ff3d00");
    public static final Color MENU_COLOR = Color.decode("#e0e0e0");
    public static final Color DARK_COLOR = Color.decode("#212121");
    public static final Color CARD_COLOR = Color.decode("#ffffff");
    public static final Color CARD_BORDER_COLOR = Color.decode("#c2c2c2");
    //public static final Color CONTAINER_COLOR = Color.decode("#fbe9e7");
    public static final Color CONTAINER_COLOR = Color.decode("#f5f5f5");
    
    //#f5f5f5
        
}
