package org.orangeplayer.playerdesktop.sys;

public enum SessionKey {
    CONTROLLER, GAIN, CUSTOM_PRIMARY_COLOR, CUSTOM_SECUNDARY_COLOR, DARK_MODE
    , FILE_CARD_MODEL_ROOT;
}
