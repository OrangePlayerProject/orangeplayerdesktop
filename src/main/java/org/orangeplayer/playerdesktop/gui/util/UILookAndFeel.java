/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.orangeplayer.playerdesktop.gui.util;

/**
 *
 * @author martin
 */
public enum UILookAndFeel {
    DEFAULT, SYSTEM, METAL, NIMBUS, GTK, MATERIAL, MAC
}
